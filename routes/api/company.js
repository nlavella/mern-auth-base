const express = require("express");
const router = express.Router();
const auth = require("../../middleware/auth");
const { check, validationResult } = require("express-validator");
const util = require("util");

const Company = require("../../models/Company");

// @route   GET api/company/current
// @desc    Get current users company
// @access  Private
router.get("/current", auth, async (req, res) => {
  try {
    const company = await Company.findOne({ _id: req.user.company_id });

    if (!company) {
      return res.status(400).json({ msg: "There is no company for this user" });
    }

    res.json(company);
  } catch (err) {
    console.error(err.message);
    res.status(500).send("Server Error");
  }
});

// @route   POST api/company
// @desc    Create or update copmany
// @access  Private
router.post(
  "/",
  [auth, [check("name", "Name is required").not().isEmpty()]],
  async (req, res) => {
    // Check validation for errors
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    const { id, name, logo } = req.body;

    // Build company object
    const companyFields = {};
    companyFields.name = name;
    companyFields.logo = logo;

    try {
      let company = await Company.findOne({ _id: id });

      if (company) {
        companyFields.userEdited = req.user.id;
        companyFields.dateEdited = Date.now();

        // Update
        company = await Company.findOneAndUpdate(
          { _id: id },
          { $set: companyFields },
          { new: true }
        );

        return res.json(company);
      } else {
        companyFields.userAdded = req.user.id;
        companyFields.userEdited = req.user.id;
        companyFields.dateAdded = Date.now();
        companyFields.dateEdited = Date.now();

        // Create
        company = new Company(companyFields);

        await company.save();
        res.json(company);
      }
    } catch (err) {
      console.error(err.message);
      res.status(500).send("Server error");
    }
  }
);

// @route   GET api/company
// @desc    Get all companies
// @access  Private
router.get("/", auth, async (req, res) => {
  try {
    const companies = await Company.find();
    res.json(companies);
  } catch (err) {
    console.error(err.message);
    res.status(500).send("Server Error");
  }
});

// @route   GET api/company/single/:company_id
// @desc    Get company by company ID
// @access  Private
router.get("/single/:company_id", auth, async (req, res) => {
  try {
    const company = await Company.findOne({ _id: req.params.company_id });

    if (!company) {
      res.status(400).json({ msg: "Company not found." });
    }

    res.json(company);
  } catch (err) {
    console.error(err.message);
    if (err.kind == "ObjectId") {
      res.status(400).json({ msg: "Company not found." });
    }
    res.status(500).send("Server Error");
  }
});

// @route   DELETE api/company/:company_id
// @desc    Delete company, communities & msc
// @access  Private
router.delete("/:company_id", auth, async (req, res) => {
  try {
    // Remove communities
    //await Compnay.findOneAndRemove({ company: req.params.company_id });

    // Remove company
    await Company.findOneAndRemove({ _id: req.params.company_id });

    res.json({ msg: "Company removed." });
  } catch (err) {
    console.error(err.message);
    res.status(500).send("Server Error");
  }
});

module.exports = router;

/*

router.get("/", auth, async (req, res) => {
  try {
  } catch (err) {
    console.error(err.message);
    res.status(500).send("Server Error");
  }
});

*/
