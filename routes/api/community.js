const express = require("express");
const router = express.Router();
const auth = require("../../middleware/auth");
const { check, validationResult } = require("express-validator");
const util = require("util");

const Community = require("../../models/Community");
const User = require("../../models/User");

// @route   POST api/community
// @desc    Create or update community
// @access  Private
router.post(
  "/",
  [
    auth,
    [
      check("name", "Name is required").not().isEmpty(),
      check("company_id", "Company is required").not().isEmpty(),
    ],
  ],
  async (req, res) => {
    // Check validation for errors
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    const {
      id,
      name,
      latitude,
      longitude,
      address,
      city,
      state,
      zipcode,
      company_id,
      status,
      assignedUsers,
    } = req.body;

    // Build company object
    const communityFields = {};
    communityFields.id = id;
    communityFields.name = name;
    communityFields.latitude = latitude;
    communityFields.longitude = longitude;
    communityFields.address = address;
    communityFields.city = city;
    communityFields.state = state;
    communityFields.zipcode = zipcode;
    communityFields.company_id = company_id;
    communityFields.status = status;

    try {
      let community = await Community.findOne({ _id: communityFields.id });

      if (community) {
        communityFields.userEdited = req.user.id;
        communityFields.dateEdited = Date.now();

        // Update
        community = await Community.findOneAndUpdate(
          { _id: id },
          { $set: communityFields },
          { new: true }
        );

        // Assign Users
        await assignedUsers.forEach(async (user) => {
          try {
            // Check if user already exists
            let _user = await User.findOne({ _id: user.user._id });

            if (_user) {
              // Build user object
              const userFields = {};
              userFields.userEdited = req.user.id;
              userFields.dateEdited = Date.now();

              // Add community if not already
              /*const filtered = user.communities.filter(
              (item) => item === community_id
            );*/

              userFields.communities = user.user.communities;

              if (user.access) {
                // gets access
                if (
                  userFields.communities.filter(
                    (com) => com._id === communityFields.id
                  ) <= 0
                ) {
                  console.log("true");
                  // add if not already
                  userFields.communities.unshift(communityFields.id);
                }
              } else {
                //remove access
                userFields.communities = userFields.communities.filter(
                  function (el) {
                    return el._id != communityFields.id;
                  }
                );
              }

              // Save user
              await User.findOneAndUpdate(
                { _id: user.user._id },
                { $set: userFields },
                { new: true }
              );
            }
          } catch (err) {
            console.error(err.message);
            res.status(500).send("Server error");
          }
        });

        return res.json(community);
      } else {
        communityFields.userAdded = req.user.id;
        communityFields.userEdited = req.user.id;
        communityFields.dateAdded = Date.now();
        communityFields.dateEdited = Date.now();

        // Create
        community = new Community(communityFields);

        await community.save();
        res.json(community);
      }
    } catch (err) {
      console.error(err.message);
      res.status(500).send("Server error");
    }
  }
);

// @route   GET api/community
// @desc    Get all communities
// @access  Private
router.get("/", auth, async (req, res) => {
  try {
    const communities = await Community.find().populate("company_id");
    res.json(communities);
  } catch (err) {
    console.error(err.message);
    res.status(500).send("Server Error");
  }
});

// @route   GET api/community/single/:community_id
// @desc    Get community by community ID
// @access  Private
router.get("/single/:community_id", auth, async (req, res) => {
  try {
    const community = await Community.findOne({
      _id: req.params.community_id,
    }).populate("company_id");

    if (!community) {
      res.status(400).json({ msg: "Community not found." });
    }

    res.json(community);
  } catch (err) {
    console.error(err.message);
    if (err.kind == "ObjectId") {
      res.status(400).json({ msg: "Community not found." });
    }
    res.status(500).send("Server Error");
  }
});

// @route   DELETE api/company/:company_id
// @desc    Delete company, communities & msc
// @access  Private
router.delete("/:community_id", auth, async (req, res) => {
  try {
    // Remove community
    await Community.findOneAndRemove({ _id: req.params.community_id });

    res.json({ msg: "Community removed." });
  } catch (err) {
    console.error(err.message);
    res.status(500).send("Server Error");
  }
});

module.exports = router;
