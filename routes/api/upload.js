const express = require("express");
const router = express.Router();
const path = require("path");
const auth = require("../../middleware/auth");

//file upload
const config = require("config");
const awsAccessKey = config.get("awsAccessKey");
const awsSecretKey = config.get("awsSecretKey");
const awsRegion = config.get("awsRegion");
const awsBucketName = config.get("awsBucketName");
const aws = require("aws-sdk");
const multer = require("multer");
const multerS3 = require("multer-s3");
aws.config.update({
  accessKeyId: awsAccessKey,
  secretAccessKey: awsSecretKey,
  region: awsRegion,
});
const s3 = new aws.S3();

var upload = multer({
  storage: multerS3({
    s3: s3,
    bucket: awsBucketName,
    acl: "public-read",
    metadata: function (req, file, cb) {
      cb(null, { fieldName: file.fieldname });
    },
    key: function (req, file, cb) {
      cb(
        null,
        path.parse(file.originalname).name +
          "-" +
          Date.now() +
          path.extname(file.originalname)
      );
    },
  }),
  fileFilter: function (req, file, cb) {
    checkFileType(file, cb);
  },
});

const singleUpload = upload.single("file");

//check filetype
function checkFileType(file, cb) {
  //allow ext
  const filetypes = /jpeg|jpg|png|gif/;
  //check ext
  const extname = filetypes.test(path.extname(file.originalname).toLowerCase());
  //check mime
  const mimetype = filetypes.test(file.mimetype);

  if (mimetype && extname) {
    return cb(null, true);
  } else {
    return cb("Error: Images only.");
  }
}

// @route   POST api/upload
// @desc    upload file
// @access  Private
router.post("/", auth, async (req, res) => {
  singleUpload(req, res, (err) => {
    if (err) {
      console.log(err);
      res.status(500).send("Server Error");
    } else {
      console.log(res);
      return res.json({ filePath: req.file.location });
    }
  });
});

module.exports = router;
