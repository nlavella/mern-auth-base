const express = require("express");
const router = express.Router();
const auth = require("../../middleware/auth");
const resetAuth = require("../../middleware/resetAuth");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const config = require("config");
const { check, validationResult } = require("express-validator");
const nodemailer = require("nodemailer");
const rootURL = config.get("rootURL");

// @route   GET api/auth
// @desc    Test route
// @access  Public
router.get("/", auth, async (req, res) => {
  try {
    const user = await User.findById(req.user.id)
      .select("-password")
      .populate("company_id")
      .populate("communities");
    res.json(user);
  } catch (err) {
    console.error(err.message);
    res.status(500).send("Server error");
  }
});

// @route   POST api/auth
// @desc    Authenticate user & get token
// @access  Public
router.post(
  "/",
  [
    // Validates data
    check("email", "Please include a valid email").isEmail(),
    check("password", "Password is required").exists(),
  ],
  async (req, res) => {
    // Check validation for erros
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    const { email, password } = req.body;

    try {
      // Check if user already exists
      let user = await User.findOne({ email })
        .populate("company_id")
        .populate("communities");

      if (!user) {
        return res
          .status(400)
          .json({ errors: [{ msg: "Invalid credentials" }] });
      }

      // Check password
      const isMatch = await bcrypt.compare(password, user.password);
      if (!isMatch) {
        return res
          .status(400)
          .json({ errors: [{ msg: "Invalid credentials" }] });
      }

      // Return jsonwebtoken
      const payload = {
        user: {
          id: user.id,
          company_id: user.company_id,
          access_level: user.access_level,
        },
      };

      jwt.sign(
        payload,
        config.get("jwtSecret"),
        { expiresIn: 360000 },
        (err, token) => {
          if (err) throw err;
          res.json({ token });
        }
      );
    } catch (err) {
      console.error(err.message);
      res.status(500).send("Server error");
    }
  }
);

// @route   POST api/auth/resetRequest
// @desc    Request reset password link
// @access  Public
router.post(
  "/resetRequest",
  [
    // Validates data
    check("email", "Please include a valid email").isEmail(),
  ],
  async (req, res) => {
    // Check validation for errors
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    const { email } = req.body;

    try {
      // Check if user already exists
      let user = await User.findOne({ email });

      if (!user) {
        return res
          .status(400)
          .json({ errors: [{ msg: "Invalid credentials" }] });
      }

      // Return jsonwebtoken
      const payload = {
        id: user.id,
      };

      jwt.sign(
        payload,
        config.get("jwtSecret"),
        { expiresIn: 36000 },
        (err, token) => {
          if (err) throw err;

          // email password
          const transporter = nodemailer.createTransport({
            host: "smtp.gmail.com",
            port: 587,
            secure: false,
            auth: {
              user: config.get("nodemailerUser"),
              pass: config.get("nodemailerPassword"),
            },
          });

          const mailOptions = {
            from: "immergeinteractive@gmail.com",
            to: `${email}`,
            subject: "Reset mern-auth-base Password",
            text: `Link to reset password: \n\n ${
              rootURL + "reset-password/" + token
            }`,
          };

          //

          transporter.sendMail(mailOptions, (err, response) => {
            if (err) {
              return res
                .status(400)
                .json({ errors: [{ msg: "Email failed" }] });
            } else {
              res.json({ msg: "sucess" });
            }
          });
        }
      );
    } catch (err) {
      console.error(err.message);
      res.status(500).send("Server error");
    }
  }
);

// @route   POST api/auth/reset
// @desc    Reset password
// @access  Private
router.post(
  "/reset",
  [
    resetAuth,
    [
      // Validates data
      check(
        "password",
        "Please enter a password with 6 or more characters"
      ).isLength({ min: 6 }),
    ],
  ],
  async (req, res) => {
    // Check validation for errors
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    const { password } = req.body;

    try {
      // Check if user already exists
      let user = await User.findOne({ _id: req.id });

      if (!user) {
        return res.status(400).json({ errors: [{ msg: "Invalid link" }] });
      }

      // Build user object
      const userFields = {};
      userFields.userEdited = req.id;
      userFields.dateEdited = Date.now();

      // Encrypt password
      const salt = await bcrypt.genSalt(10);
      userFields.password = await bcrypt.hash(password, salt);

      // Save user
      await User.findOneAndUpdate(
        { _id: req.id },
        { $set: userFields },
        { new: true }
      );

      return res.json({ msg: "Password reset" });
    } catch (err) {
      console.error(err.message);
      res.status(500).send("Server error");
    }
  }
);

module.exports = router;
