const express = require("express");
const router = express.Router();
const auth = require("../../middleware/auth");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const config = require("config");
const { check, validationResult } = require("express-validator");

const User = require("../../models/User");

// @route   POST api/users/add
// @desc    Register user & login - OG Public
// @access  Public
router.post(
  "/regLog",
  [
    // Validates data
    check("name", "Name is required").not().isEmpty(),
    check("email", "Please include a valid email").isEmail(),
    check(
      "password",
      "Please enter a password with 6 or more characters"
    ).isLength({ min: 6 }),
  ],
  async (req, res) => {
    // Check validation for errors
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    const { name, email, password } = req.body;

    try {
      // Check if user already exists
      let user = await User.findOne({ email });

      if (user) {
        return res
          .status(400)
          .json({ errors: [{ msg: "User already exists" }] });
      }

      user = new User({
        name,
        email,
        password,
      });

      // Encrypt password
      const salt = await bcrypt.genSalt(10);
      user.password = await bcrypt.hash(password, salt);

      // Save user
      await user.save();

      // Return jsonwebtoken
      const payload = {
        user: {
          id: user.id,
        },
      };

      jwt.sign(
        payload,
        config.get("jwtSecret"),
        { expiresIn: 3600 },
        (err, token) => {
          if (err) throw err;
          res.json({ token });
        }
      );
    } catch (err) {
      console.error(err.message);
      res.status(500).send("Server error");
    }
  }
);

// @route   POST api/users/add
// @desc    Register user
// @access  Private
router.post(
  "/",
  [
    auth,
    [
      // Validates data
      check("name", "Name is required").not().isEmpty(),
      check("email", "Please include a valid email").isEmail(),
      check(
        "password",
        "Please enter a password with 6 or more characters"
      ).isLength({ min: 6 }),
    ],
  ],
  async (req, res) => {
    // Check validation for errors
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    const {
      name,
      email,
      access_level,
      password,
      avatar,
    } = req.body;

    try {
      // Check if user already exists
      let user = await User.findOne({ email });

      if (user) {
        return res
          .status(400)
          .json({ errors: [{ msg: "User already exists" }] });
      }

      user = new User({
        name,
        email,
        access_level,
        password,
        avatar,
        userAdded: req.user.id,
        userEdited: req.user.id,
        dateAdded: Date.now(),
        dateEdited: Date.now(),
      });

      // Encrypt password
      const salt = await bcrypt.genSalt(10);
      user.password = await bcrypt.hash(password, salt);

      // Save user
      await user.save();
      res.json(user);
    } catch (err) {
      console.error(err.message);
      res.status(500).send("Server error");
    }
  }
);

// @route   POST api/users/update
// @desc    Update user
// @access  Private
router.post(
  "/update/",
  [
    auth,
    [
      // Validates data
      check("name", "Name is required").not().isEmpty(),
    ],
  ],
  async (req, res) => {
    // Check validation for errors
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    const { id, name, access_level, avatar } = req.body;

    try {
      // Check if user already exists
      let user = await User.findOne({ _id: id });

      if (user) {
        // Build user object
        const userFields = {};
        userFields.name = name;
        userFields.access_level = access_level;
        userFields.avatar = avatar;
        userFields.userEdited = req.user.id;
        userFields.dateEdited = Date.now();

        // Save user
        await User.findOneAndUpdate(
          { _id: id },
          { $set: userFields },
          { new: true }
        );

        res.json(user);
      }
    } catch (err) {
      console.error(err.message);
      res.status(500).send("Server error");
    }
  }
);

// @route   POST api/users/assign
// @desc    Assign community to user
// @access  Private
router.post(
  "/assign/",
  [
    auth,
    [
      // Validates data
      check("user_id", "User is required").not().isEmpty(),
      check("community_id", "Community is required").not().isEmpty(),
    ],
  ],
  async (req, res) => {
    // Check validation for errors
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    const { user_id, community_id } = req.body;

    try {
      // Check if user already exists
      let user = await User.findOne({ _id: user_id });

      if (user) {
        // Build user object
        const userFields = {};
        userFields.userEdited = req.user.id;
        userFields.dateEdited = Date.now();

        // Add community if not already
        /*const filtered = user.communities.filter(
          (item) => item === community_id
        );*/

        userFields.communities = user.communities;

        if (!userFields.communities.includes(community_id)) {
          userFields.communities.unshift(community_id);
        } else {
          return res
            .status(400)
            .json({ errors: [{ msg: "Community already assigned to user" }] });
        }

        // Save user
        await User.findOneAndUpdate(
          { _id: user_id },
          { $set: userFields },
          { new: true }
        );

        res.json(user);
      }
    } catch (err) {
      console.error(err.message);
      res.status(500).send("Server error");
    }
  }
);

// @route   GET api/users
// @desc    Get all users
// @access  Private
router.get("/", auth, async (req, res) => {
  try {
    const users = await User.find()
      .populate("communities");
    res.json(users);
  } catch (err) {
    console.error(err.message);
    res.status(500).send("Server Error");
  }
});

// @route   GET api/users/single/:user_id
// @desc    Get user by user ID
// @access  Private
router.get("/single/:user_id", auth, async (req, res) => {
  try {
    const user = await User.findOne({
      _id: req.params.user_id,
    })
      .populate("communities");

    if (!user) {
      res.status(400).json({ msg: "User not found." });
    }

    res.json(user);
  } catch (err) {
    console.error(err.message);
    if (err.kind == "ObjectId") {
      res.status(400).json({ msg: "User not found." });
    }
    res.status(500).send("Server Error");
  }
});

module.exports = router;
