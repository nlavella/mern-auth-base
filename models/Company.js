const mongoose = require("mongoose");

const CompanySchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  logo: {
    type: String,
  },
  userAdded: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "user",
  },
  userEdited: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "user",
  },
  dateAdded: {
    type: Date,
  },
  dateEdited: {
    type: Date,
    default: Date.now,
  },
});

module.exports = Company = mongoose.model("company", CompanySchema);
