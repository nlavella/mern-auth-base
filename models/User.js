const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  email: {
    type: String,
    required: true,
    unique: true,
  },
  access_level: {
    type: Number,
    required: false,
  },
  communities: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: "community",
    },
  ],
  password: {
    type: String,
    required: true,
  },
  avatar: {
    type: String,
  },
  userAdded: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "user",
  },
  userEdited: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "user",
  },
  dateAdded: {
    type: Date,
  },
  dateEdited: {
    type: Date,
    default: Date.now,
  },
});

module.exports = User = mongoose.model("user", userSchema);
