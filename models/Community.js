const mongoose = require("mongoose");

const CommunitySchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  latitude: {
    type: Number,
  },
  longitude: {
    type: Number,
  },
  address: {
    type: String,
  },
  city: {
    type: String,
  },
  state: {
    type: String,
  },
  zipcode: {
    type: Number,
  },
  company_id: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "company",
    required: true,
  },
  status: {
    type: Number,
    required: true,
  },
  userAdded: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "user",
  },
  userEdited: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "user",
  },
  dateAdded: {
    type: Date,
  },
  dateEdited: {
    type: Date,
    default: Date.now,
  },
});

module.exports = Community = mongoose.model("community", CommunitySchema);
