FROM node:8
WORKDIR /app

COPY package.json /app
RUN npm install
RUN npm run client-install
COPY . /app

EXPOSE 3000
CMD ["npm", "run", "dev"]