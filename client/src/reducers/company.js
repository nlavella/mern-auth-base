import {
  GET_COMPANY,
  GET_COMPANIES,
  COMPANY_ERROR,
  CLEAR_COMPANY,
} from "../actions/types";

const initalState = {
  company: null,
  companies: [],
  loading: true,
  error: {},
};

export default function (state = initalState, action) {
  const { type, payload } = action;

  switch (type) {
    case GET_COMPANY:
      return {
        ...state,
        company: payload,
        loading: false,
      };
    case GET_COMPANIES:
      return {
        ...state,
        companies: payload,
        loading: false,
      };
    case COMPANY_ERROR:
      return {
        ...state,
        error: payload,
        loading: false,
      };
    case CLEAR_COMPANY:
      return {
        ...state,
        company: null,
        loading: false,
      };
    default:
      return state;
  }
}
