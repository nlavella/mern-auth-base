export const adminLevels = [
  {
    name: "Super Admin",
    level: 0,
  },
  {
    name: "Admin",
    level: 1,
  },
  {
    name: "User",
    level: 2,
  },
];
