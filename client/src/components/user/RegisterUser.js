import React, { Fragment, useState, useEffect } from "react";
import { Redirect, withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { setAlert } from "../../actions/alert";
import { register } from "../../actions/user";
import PropTypes from "prop-types";
import * as Constants from "../../utils/constants";

const RegisterUser = ({
  setAlert,
  register,
  auth: { user, isAuthenticated },
  history,
}) => {
  useEffect(() => {
  }, []);

  const [formData, setFormData] = useState({
    name: "",
    email: "",
    access_level: "",
    password: "",
    password2: "",
  });

  const {
    name,
    email,
    access_level,
    password,
    password2,
  } = formData;

  const onChange = (e) =>
    setFormData({ ...formData, [e.target.name]: e.target.value });

  const onSubmit = (e) => {
    e.preventDefault();
    if (password !== password2) {
      setAlert("Passwords do not match", "danger", 3000);
    } else {
      register({ name, email, access_level, password }, history);
    }
  };

  // admin options, admin shows select, other autosets to same co
  const adminSelect = () => {
    const availableLevels = Constants.adminLevels.filter(
      (admin) => admin.level >= user.access_level
    );

    return (
      <select
        placeholder="Access Level"
        name="access_level"
        value={access_level}
        onChange={(e) => onChange(e)}
      >
        <option>Access Level</option>
        {availableLevels.length > 0
          ? availableLevels.map((admin) => (
              <option value={admin.level} key={admin.level}>
                {admin.name}
              </option>
            ))
          : null}
      </select>
    );
  };

  // Redirect if logged in
  if (!isAuthenticated || !user) {
    return <Redirect to="/dashboard" />;
  }

  return (
    <Fragment>
      <h2>Create User</h2>
      <form onSubmit={(e) => onSubmit(e)}>
        <div className="form-group">
          <input
            type="text"
            placeholder="Name"
            name="name"
            value={name}
            onChange={(e) => onChange(e)}
          />
        </div>
        <div className="form-group">
          <input
            type="email"
            placeholder="Email Address"
            name="email"
            value={email}
            onChange={(e) => onChange(e)}
          />
        </div>
        <div className="form-group">{adminSelect()}</div>
        <div className="form-group">
          <input
            type="password"
            placeholder="Password"
            name="password"
            value={password}
            onChange={(e) => onChange(e)}
          />
        </div>
        <div className="form-group">
          <input
            type="password"
            placeholder="Confirm Password"
            name="password2"
            value={password2}
            onChange={(e) => onChange(e)}
          />
        </div>

        <button type="submit" className="btn">
          Add
        </button>
      </form>
    </Fragment>
  );
};

RegisterUser.propTypes = {
  setAlert: PropTypes.func.isRequired,
  register: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  auth: state.auth,
});

export default connect(mapStateToProps, { setAlert, register })(
  withRouter(RegisterUser)
);
