import React, { Fragment, useState, useEffect } from "react";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { setAlert } from "../../actions/alert";
import { getUserById, updateUser } from "../../actions/user";
import PropTypes from "prop-types";
import * as Constants from "../../utils/constants";

import FileUpload from "../layout/FileUpload";

const EditUser = ({
  match,
  setAlert,
  getUserById,
  getCompanies,
  updateUser,
  user: { user, loading },
  auth,
  history,
}) => {
  useEffect(() => {
    getUserById(match.params.id);
  }, [getUserById, match]);

  useEffect(() => {
    setFormData({
      id: loading || !user ? null : user._id,
      name: loading || !user ? "" : user.name,
      access_level: loading || !user ? "" : user.access_level,
      avatar: loading || !user ? "" : user.avatar,
    });
  }, [user, loading]);

  const [formData, setFormData] = useState({
    id: "",
    name: "",
    access_level: "",
    avatar: "",
  });

  const { id, name, access_level, avatar } = formData;

  const onChange = (e) =>
    setFormData({ ...formData, [e.target.name]: e.target.value });

  const onSubmit = (e) => {
    e.preventDefault();
    updateUser({ id, name, access_level, avatar }, history);
  };

  const onImageUpload = (url) => {
    setFormData({ ...formData, avatar: url });
  };

  // admin options, admin shows select, other autosets to same co
  const adminSelect = () => {
    if (auth.user) {
      const availableLevels = Constants.adminLevels.filter(
        (admin) => admin.level >= auth.user.access_level
      );

      return (
        <select
          placeholder="Access Level"
          name="access_level"
          value={access_level}
          onChange={(e) => onChange(e)}
        >
          <option>Access Level</option>
          {availableLevels.length > 0
            ? availableLevels.map((admin) => (
                <option value={admin.level} key={admin.level}>
                  {admin.name}
                </option>
              ))
            : null}
        </select>
      );
    }
  };

  return (
    <Fragment>
      <h2>Edit User</h2>
      <FileUpload currentFile={avatar} onComplete={onImageUpload} />
      <form onSubmit={(e) => onSubmit(e)}>
        <div className="form-group">
          <input
            type="text"
            placeholder="Name"
            name="name"
            value={name}
            onChange={(e) => onChange(e)}
          />
        </div>
        <div className="form-group">{adminSelect()}</div>
        <button type="submit" className="btn">
          Update
        </button>
      </form>
    </Fragment>
  );
};

EditUser.propTypes = {
  setAlert: PropTypes.func.isRequired,
  getUserById: PropTypes.func.isRequired,
  updateUser: PropTypes.func.isRequired,
  user: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  user: state.user,
  auth: state.auth,
});

export default connect(mapStateToProps, {
  setAlert,
  getUserById,
  updateUser
})(withRouter(EditUser));
