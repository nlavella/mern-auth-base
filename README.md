# NL: MERN-AUTH-BASE

## 1. Update configs in project file

### A. /config/default.json at a minimum with mongo db URI

### B. /config/default.json add a project specific jwtSecret

## 2. Startup dev

### 1. `npm install`

### 1b. `cd client npm install`

### 2. `docker-compose build`

### 3. `docker-compose up`

### 3. `docker exec -it <CONTAINER ID FOR REACT-FRONTEND> npm install node-sass`

** fixes issue with node-sass env **
** might also need `docker exec -it <CONTAINER ID FOR REACT_FRONTEND> npm rebuild node-sass` INSIDE /client directory and restart docker**

# Legacy README

## Startup fresh repo

### 1. `docker-compose build`

### 2. `docker-compose up`

### 3. `docker exec -it <CONTAINER ID FOR REACT-FRONTEND> npm install node-sass`

** fixes issue with node-sass env **
** might also need `docker exec -it <CONTAINER ID FOR REACT_FRONTEND> npm rebuild node-sass` and restart docker**

## Helpful commands

### `docker container ls`

Show all docker containers running

### `docker kill $ID$`

Kill container

### Heroku

Push none master branch `$ git push heroku yourbranch:master`
