const express = require("express");
const connectDB = require("./config/db");
const path = require("path");

const app = express();

// Connect Database
connectDB();

// Init Middleware
app.use(express.json({ extended: false }));

// Define Routes
app.use("/api/users", require("./routes/api/users"));
app.use("/api/auth", require("./routes/api/auth"));
app.use("/api/upload", require("./routes/api/upload"));

// App Specific Data
app.use("/api/company", require("./routes/api/company"));
app.use("/api/community", require("./routes/api/community"));

// Serve static assest in production
if (process.env.NODE_ENV === "production") {
  //Set static folder
  app.use(express.static("client/build"));

  app.get("*", (req, res) => {
    res.sendFile(path.resolve(__dirname, "client", "build", "index.html"));
  });
}

const PORT = process.env.PORT || 6200;
app.listen(PORT, () => console.log(`Server started on port ${PORT}`));
